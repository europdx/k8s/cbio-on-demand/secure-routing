### APACHE SAML Configuration


## Build
**Command:**

  `docker build -t <repo>/<image-name>:<tag>`
  
**Example:**

  `docker build -t lpivo/k8s-saml:t1 .`
  
  `docker build --build-arg SOURCE=/mylocation/secure-routing -t lpivo/k8s-saml:t1 .`
  
**Args:**

  SOURCE -> location of python app source code
  
  (default=`./secure-routing`)

## before_build.sh

Run if you dont have sp metadata which are registered on idp

! Ask perun guys !
