import os
import subprocess

from flask import Flask, request, Response
from flask import abort

app = Flask(__name__)
path_to_config_file = os.environ.get("APACHE_HOME", '/etc/apache2/sites-enabled/routes/')


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/routes', methods=['POST'])
def register_new_route():
    service_path = request.args.get('sp')
    service_fqdn = request.args.get('sf')
    _add_route(service_path, service_fqdn)

    result = reload()
    app.logger.info(result.stdout)
    if result.returncode == 0:
        return Response(status=200)

    app.logger.info(result.returncode)
    abort(500)


@app.route('/routes', methods=['DELETE'])
def remove_route():
    service_path = request.args.get('sp')
    _remove_config(service_path)

    result = reload()
    app.logger.info(result.stdout)
    if result.returncode == 0:
        return Response(status=200)

    app.logger.warn('apache failed to reload')
    abort(500)


def _add_route(service_path, service_fqdn):
    with open(path_to_config_file + service_path + '.conf', 'w') as output:
        output.write("<Location /" + service_path + " >\n" +
                     "\n" +
                     "  Include /etc/apache2/sites-enabled/mellon/mellon.conf\n"
                     "\n" +
                     "  ProxyPass http://" + service_fqdn + "/" + service_path + "\n" +
                     "  ProxyPassReverse http://" + service_fqdn + "/" + service_path + "\n" +
                     "\n" +
                     "</Location>\n")


def reload():
    app.logger.info('reloading apache')
    return subprocess.run(["service", "apache2", "reload"], stdout=subprocess.PIPE)


def _remove_config(service_path):
    os.remove(path_to_config_file + service_path + '.conf')


if __name__ == '__main__':
    app.run()
