#!/bin/sh
mellon=/etc/apache2/mellon
mellonconf=/etc/apache2/sites-enabled/mellon/

cd /$mellon

# move mellon metadata
mv /sp-metadata.xml /${mellon}/sp-metadata.xml
mv /idp-metadata.xml $mellon
mv ${mellon}/*.cert ${mellon}/sp_cert.pem
mv ${mellon}/*.key ${mellon}/sp_key.pem

# mellon conf
mkdir $mellonconf
mv /mellon.conf ${mellonconf}

# create fqdn.conf
echo "Define FQDN ${HOST}" >/etc/apache2/fqdn.conf
echo "Define EMAILADMIN ${ADMIN_USER}" >> /etc/apache2/fqdn.conf

# run Apache
#/usr/sbin/apache2ctl -D FOREGROUND

#run supervisor
/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
